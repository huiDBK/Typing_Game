g_debug_flag = 0 # 默认非调试

def ws_setDebugging():
	global g_debug_flag
	g_debug_flag = 1

def ws_debug(*args, **kwargs):
	global g_debug_flag
	if g_debug_flag:
		print(*args, **kwargs)
